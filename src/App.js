import React from 'react';
import './App.css';

import Header from "./components/Header.js";
import List from "./components/List.js";
import AddElement from './components/AddElement';
import Contact from "components/Contact";

import { EmptyContact } from "models/Contact.js";

import { ContactsService } from "services/Contacts.js"

function App() {
  return (
    <div className="App">
      <Header />
      <AddElement
        service={ContactsService}
        handleCreation={EmptyContact}
      >
        <Contact editable={true}/>
      </AddElement>

      <List
        service={ContactsService}
        identifier="name"
        deletable={true}
      >
        <Contact />
      </List>
    </div>
  );
}

export default App;
