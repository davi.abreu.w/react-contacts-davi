import React from "react";

export class ChildrenHelper {

  static instance_one(root, props = {}) {
    return React.cloneElement(
      root.props.children,
      props
    )
  }

}