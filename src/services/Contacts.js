import { Contact } from "models/Contact.js"
import { Observed } from "../models/Observed";

export class ContactsService extends Observed {

  static contacts = [
    new Contact("Jo", "123456"),
    new Contact("Blue", "321")
  ];

  static get_list() {
    return this.contacts;
  }

  static add(contact) {
    this.contacts.push(contact);
    this.notify();
  }

  static delete(contact) {
    let index = this.contacts.indexOf(contact);
    this.contacts.splice(index, 1);
    this.notify();
  }
}

