export class Contact {
  constructor(name, phone) {
    this.name = name;
    this.phone = phone;
  }
}

export function EmptyContact() {
  return new Contact("", "");
}
