export class Observed {
  static source = "Default";
  static observers = [];

  static register(observer){
    this.observers.push(observer);
  }

  static unregister(observer){
    let index = this.observers.indexOf(observer);
    this.observers.splice(index, 1);
  }

  static notify(event = null){
    this.observers.map(
      observer => observer.update(this.source, event)
    )
  }
}