import React, { Component } from "react";

export default class Input extends Component {

  render() {
    return (
      <input className="Input"
        type={this.props.type}
        value={this.props.value}
        placeholder={this.props.placeholder}
        onChange={this.props.handleChange}
      />
    );
  }
  
}