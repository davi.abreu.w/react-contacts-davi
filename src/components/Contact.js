import React, { Component } from "react";

import Input from "components/Input";

import { InputHelper } from "helpers/Input";

export default class Contact extends Component {

  constructor(props) {
    super(props);
    this.state = {element: props.element};
  }

  render() {
    if (this.props.editable) {
      return (
        <div>
          <Input
            type="text"
            value={this.state.element.name}
            placeholder="Nome"
            handleChange={
              InputHelper.bind_element(this, "element", "name")
            }
          />
          <Input
            type="text"
            value={this.state.element.phone}
            placeholder="Telefone"
            handleChange={
              InputHelper.bind_element(this, "element", "phone")
            }
          />
        </div>
      )
    }

    return (
      <div key={this.state.element.name}>
        <span> Nome: {this.state.element.name} </span> |
        <span> Telefone: {this.state.element.phone} </span>
      </div>
    )
  }

}