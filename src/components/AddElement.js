import React, { Component } from "react";

import Button from "components/Button";

import { ChildrenHelper } from "../helpers/Children";

export default class AddElement extends Component {
  constructor(props) {
    super(props);
    this.state = { element: null };
  }

  create() {
    this.setState({ element: this.props.handleCreation() });
  }

  confirm() {
    this.props.service.add(this.state.element);
    this.setState({ element: null });
  }

  cancel() {
    this.setState({ element: null });
  }

  render() {
    if (this.state.element == null) {
      return (
        <Button
          handleClick={this.create.bind(this)}
          label="Adicionar"
        />
      )
    }
    return (
      <div>
        {
          this.state.element &&
          ChildrenHelper.instance_one(this, {element: this.state.element})
        }
        <Button
          handleClick={this.confirm.bind(this)}
          label="Confirmar"
        />
        <Button
          handleClick={this.cancel.bind(this)}
          label="Cancelar"
        />
      </div>
    );
  }
}